package com.parkify.backend.service;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingException;
import com.google.firebase.messaging.Message;
import com.parkify.backend.domain.ParkingDetail;
import com.parkify.backend.domain.ParkingEvent;
import com.parkify.backend.domain.Payment;
import com.parkify.backend.domain.dto.request.PushNotificationRequestDto;
import com.parkify.backend.domain.dto.request.PushNotificationRequestDto.Data;
import com.parkify.backend.domain.dto.request.PushNotificationRequestDto.Notification;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.*;
import java.util.Arrays;

@Service
public class PushNotificationService {

    private static final String PROJECT_ID = "parkify-e052a";
    private static final String BASE_URL = "https://fcm.googleapis.com";
    private static final String FCM_SEND_ENDPOINT = BASE_URL + "/v1/projects/" + PROJECT_ID + "/messages:send";

    private static final String MESSAGING_SCOPE = "https://www.googleapis.com/auth/firebase.messaging";
    private static final String[] SCOPES = {MESSAGING_SCOPE};
    private static FirebaseApp firebaseApp;

    private RestTemplate restTemplate;

    public PushNotificationService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    private FirebaseApp getApp() {

        if (this.firebaseApp == null) {
            InputStream serviceAccount =
                    this.getClass().getResourceAsStream("/serviceAccount.json");

            FirebaseOptions options = null;
            try {
                options = new FirebaseOptions.Builder()
                        .setCredentials(GoogleCredentials.fromStream(serviceAccount))
                        .setDatabaseUrl("https://parkify-e052a.firebaseio.com")
                        .build();
            } catch (IOException e) {
                e.printStackTrace();
            }

            this.firebaseApp = FirebaseApp.initializeApp(options);
        }


        return this.firebaseApp;
    }

    public String sendRegisterEventNotification(ParkingEvent parkingEvent) throws IOException, FirebaseMessagingException {
        PushNotificationRequestDto pushNotificationRequestDto = buildPushNotificationRequestDto(
                parkingEvent.getUser().getRegistrationId(),
                "PUBLIC",
                "body",
                "title",
                parkingEvent.getId(),
                parkingEvent.getParkingSpot().getParking().getParkingDetail(),
                null);
        HttpHeaders httpHeaders = buildHttpHeaders();
        HttpEntity<PushNotificationRequestDto> httpEntity = new HttpEntity<>(pushNotificationRequestDto, httpHeaders);

        Message message = Message.builder()
                .setToken(parkingEvent.getUser().getRegistrationId())
                .setNotification(new com.google.firebase.messaging.Notification("title", "body"))
                .putData("eventType", "PUBLIC")
                .putData("parkingEventId", parkingEvent.getId().toString())
                .putData("openFrom", parkingEvent.getParkingSpot().getParking().getParkingDetail().getOpenFrom().toString())
                .putData("openTo", parkingEvent.getParkingSpot().getParking().getParkingDetail().getOpenTo().toString())
                .putData("hourlyFee", parkingEvent.getParkingSpot().getParking().getParkingDetail().getHourlyFee().toString())
                .putData("currency", parkingEvent.getParkingSpot().getParking().getParkingDetail().getCurrency().toString())
                .build();

//        try {
            String response = FirebaseMessaging.getInstance(this.getApp()).send(message);
            System.out.println(response);
//        } catch (FirebaseMessagingException e) {
//            e.printStackTrace();
//        }

//        return restTemplate.postForObject(FCM_SEND_ENDPOINT, httpEntity, String.class);
        return response;
    }

    public String sendPaymentEventNotification(ParkingEvent parkingEvent) throws IOException, FirebaseMessagingException {

        PushNotificationRequestDto pushNotificationRequestDto = buildPushNotificationRequestDto(
                parkingEvent.getUser().getRegistrationId(),
                "PAYMENT",
                "body",
                "title",
                parkingEvent.getId(),
                null,
                parkingEvent.getPayment());
        HttpHeaders httpHeaders = buildHttpHeaders();
        HttpEntity<PushNotificationRequestDto> httpEntity = new HttpEntity<>(pushNotificationRequestDto, httpHeaders);

        Message message = Message.builder()
                .setToken(parkingEvent.getUser().getRegistrationId())
                .setNotification(new com.google.firebase.messaging.Notification("title", "body"))
                .putData("eventType", "PAYMENT")
                .putData("amount", parkingEvent.getPayment().getAmount().toString())
                .putData("quantity", parkingEvent.getPayment().getQuantity().toString())
                .putData("currency", parkingEvent.getParkingSpot().getParking().getParkingDetail().getCurrency())
                .build();

//        try {
        String response = FirebaseMessaging.getInstance(this.getApp()).send(message);
        System.out.println(response);
//        } catch (FirebaseMessagingException e) {
//            e.printStackTrace();
//        }

            return  response;
//        return restTemplate.postForObject(FCM_SEND_ENDPOINT, httpEntity, String.class);
    }

    private HttpHeaders buildHttpHeaders() throws IOException {

        String token;
        InputStream serviceAccount = this.getClass().getResourceAsStream("/serviceAccount.json");

        GoogleCredential googleCredential = GoogleCredential
                .fromStream(serviceAccount)
                .createScoped(Arrays.asList(SCOPES));
        googleCredential.refreshToken();
        token = googleCredential.getAccessToken();

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        httpHeaders.set("Authorization", "Bearer " + token);
        return httpHeaders;
    }


    private PushNotificationRequestDto buildPushNotificationRequestDto(
            String registrationId,
            String eventType,
            String body,
            String title,
            Integer parkingEventId,
            ParkingDetail parkingDetail,
            Payment payment) {


        PushNotificationRequestDto pushNotificationRequestDto = new PushNotificationRequestDto();
        Data data = new Data();
        data.setParkingEventId(parkingEventId);
        data.setEventType(eventType);
        data.setParkingDetail(parkingDetail);
        data.setPayment(payment);
        Notification notification = new Notification();
        notification.setBody(body);
        notification.setTitle(title);

        PushNotificationRequestDto.Message message = new PushNotificationRequestDto.Message();

        message.setTopic("parking");
        message.setNotification(notification);

        message.setData(data);

        pushNotificationRequestDto.setMessage(message);

        return pushNotificationRequestDto;
    }
}
