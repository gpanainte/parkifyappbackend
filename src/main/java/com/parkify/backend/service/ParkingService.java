package com.parkify.backend.service;

import com.parkify.backend.domain.ParkingEvent;
import com.parkify.backend.domain.ParkingSpot;
import com.parkify.backend.domain.Payment;
import com.parkify.backend.domain.User;
import com.parkify.backend.repository.ParkingEventRepository;
import com.parkify.backend.repository.ParkingSpotRepository;
import com.parkify.backend.repository.PaymentRepository;
import com.parkify.backend.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Objects;

@Service
public class ParkingService {

    private UserRepository userRepository;
    private ParkingSpotRepository parkingSpotRepository;
    private ParkingEventRepository parkingEventRepository;
    private PaymentRepository paymentRepository;

    @Autowired
    public ParkingService(UserRepository userRepository,
                          ParkingSpotRepository parkingSpotRepository,
                          ParkingEventRepository parkingEventRepository,
                          PaymentRepository paymentRepository) {
        this.userRepository = userRepository;
        this.parkingSpotRepository = parkingSpotRepository;
        this.parkingEventRepository = parkingEventRepository;
        this.paymentRepository = paymentRepository;
    }

    public ParkingSpot getParkingSpot(BigDecimal parkingSpotId) {
        return parkingSpotRepository.findParkingSpotById(parkingSpotId);
    }

    public ParkingEvent registerParkingEvent(BigDecimal parkingSpotId, BigDecimal tagId) {
        User user = userRepository.findUserByTagId(tagId);
        ParkingSpot parkingSpot = parkingSpotRepository.findParkingSpotById(parkingSpotId);
        ParkingEvent parkingEvent = new ParkingEvent();
        parkingEvent.setUser(user);
        parkingEvent.setParkingSpot(parkingSpot);
        parkingEvent.setStatus("DETECTED");
        return parkingEventRepository.save(parkingEvent);
    }

    public ParkingEvent updateParkingEvent(Integer parkingEventId) {
        ParkingEvent parkingEvent = parkingEventRepository.findParkingEventById(parkingEventId);
        if (Objects.equals(parkingEvent.getStatus(), "ACCEPTED")) {
            parkingEvent.setDateTo(LocalDateTime.now());
            parkingEventRepository.save(parkingEvent);
        }
        return parkingEvent;
    }

    public ParkingEvent acceptParkingEvent(Integer parkingEventId) {
        ParkingEvent parkingEvent = parkingEventRepository.findParkingEventById(parkingEventId);
        parkingEvent.setDateFrom(LocalDateTime.now());
        parkingEvent.setStatus("ACCEPTED");
        return parkingEventRepository.save(parkingEvent);
    }

    public ParkingEvent submitDuePayment(ParkingEvent parkingEvent) {
        parkingEvent.setStatus("ENDED");
        Payment payment = new Payment();
        payment.setUser(parkingEvent.getUser());
        payment.setCard(parkingEvent.getUser().getPaymentCard());
        BigDecimal quantity = getSecondsParked(parkingEvent);
        payment.setAmount(quantity.multiply(parkingEvent.getParkingSpot().getParking().getParkingDetail().getHourlyFee()));
        payment.setQuantity(quantity);
        payment.setDate(LocalDateTime.now());
        paymentRepository.save(payment);
        parkingEvent.setPayment(payment);
        return parkingEventRepository.save(parkingEvent);
    }

    private BigDecimal getSecondsParked(ParkingEvent parkingEvent) {
        return new BigDecimal(Duration.between(parkingEvent.getDateFrom(), parkingEvent.getDateTo()).toMillis() / 1000);
    }
}
