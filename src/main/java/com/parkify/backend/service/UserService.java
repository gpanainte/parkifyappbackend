package com.parkify.backend.service;

import com.parkify.backend.domain.User;
import com.parkify.backend.domain.dto.request.UpdateUserDataRequestDto;
import com.parkify.backend.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    private UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User getUser(String plateNo) {
        return userRepository.findUserByPlateNo(plateNo);
    }

    public User updateUserData(String plateNo, UpdateUserDataRequestDto updateUserDataRequestDto) {
        User user = userRepository.findUserByPlateNo(plateNo);
        if (updateUserDataRequestDto.getRegistrationId() != null) {
            user.setRegistrationId(updateUserDataRequestDto.getRegistrationId());
        }
        if (updateUserDataRequestDto.getTagId() != null) {
            user.setTagId(updateUserDataRequestDto.getTagId());
        }
        return userRepository.save(user);
    }
}
