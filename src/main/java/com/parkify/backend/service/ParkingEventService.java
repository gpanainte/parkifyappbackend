package com.parkify.backend.service;

import com.parkify.backend.domain.ParkingEvent;
import com.parkify.backend.repository.ParkingEventRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ParkingEventService {

    private ParkingEventRepository parkingEventRepository;

    @Autowired
    public ParkingEventService(ParkingEventRepository parkingEventRepository) {
        this.parkingEventRepository = parkingEventRepository;
    }

    public ParkingEvent getParkingEvent(Integer parkingEventId) {
        return parkingEventRepository.findParkingEventById(parkingEventId);
    }
}
