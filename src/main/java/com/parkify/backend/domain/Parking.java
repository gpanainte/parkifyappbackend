package com.parkify.backend.domain;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
public class Parking {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private BigDecimal id;

    private String address;

    private String type;

    @OneToOne(mappedBy = "parking")
    private ParkingDetail parkingDetail;

    public BigDecimal getId() {
        return id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public ParkingDetail getParkingDetail() {
        return parkingDetail;
    }

    public void setParkingDetail(ParkingDetail parkingDetail) {
        this.parkingDetail = parkingDetail;
    }

    @Override
    public String toString() {
        return "Parking{" +
                "id=" + id +
                ", address='" + address + '\'' +
                ", type='" + type + '\'' +
                ", parkingDetail=" + parkingDetail +
                '}';
    }
}
