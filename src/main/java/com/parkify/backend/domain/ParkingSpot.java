package com.parkify.backend.domain;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
public class    ParkingSpot {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private BigDecimal id;

    private BigDecimal spotNo;

    @ManyToOne
    private Parking parking;

    @ManyToOne
    @JoinTable(name = "park_owner",
            joinColumns = {@JoinColumn(name = "user_id")},
            inverseJoinColumns = {@JoinColumn(name = "parking_spot_id")})
    private User user;

    public BigDecimal getId() {
        return id;
    }

    public BigDecimal getSpotNo() {
        return spotNo;
    }

    public void setSpotNo(BigDecimal spotNo) {
        this.spotNo = spotNo;
    }

    public Parking getParking() {
        return parking;
    }

    public void setParking(Parking parking) {
        this.parking = parking;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
