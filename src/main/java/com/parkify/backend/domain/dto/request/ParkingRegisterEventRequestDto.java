package com.parkify.backend.domain.dto.request;

import java.math.BigDecimal;

public class ParkingRegisterEventRequestDto {
    private BigDecimal tagId;
    private Integer parkingEventId;

    public BigDecimal getTagId() {
        return tagId;
    }

    public void setTagId(BigDecimal tagId) {
        this.tagId = tagId;
    }

    public Integer getParkingEventId() {
        return parkingEventId;
    }

    public void setParkingEventId(Integer parkingEventId) {
        this.parkingEventId = parkingEventId;
    }
}
