package com.parkify.backend.domain.dto.request;

import java.math.BigDecimal;

public class UpdateUserDataRequestDto {
    private BigDecimal tagId;
    private String registrationId;

    public BigDecimal getTagId() {
        return tagId;
    }

    public void setTagId(BigDecimal tagId) {
        this.tagId = tagId;
    }

    public String getRegistrationId() {
        return registrationId;
    }

    public void setRegistrationId(String registrationId) {
        this.registrationId = registrationId;
    }
}
