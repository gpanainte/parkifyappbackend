package com.parkify.backend.domain.dto.request;

import com.parkify.backend.domain.ParkingDetail;
import com.parkify.backend.domain.Payment;

public class PushNotificationRequestDto {
    private Message message;


    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }

    public static class Notification {
        private String title;
        private String body;

        public String getBody() {
            return body;
        }

        public void setBody(String body) {
            this.body = body;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

    }

    public static class Message{
        private String topic;
        private Notification notification;
        private Data data;

        public String getTopic() {
            return topic;
        }

        public void setTopic(String topic) {
            this.topic = topic;
        }

        public Notification getNotification() {
            return notification;
        }

        public void setNotification(Notification notification) {
            this.notification = notification;
        }

        public Data getData() {
            return data;
        }

        public void setData(Data data) {
            this.data = data;
        }

    }

    public static class Data {
        private Integer parkingEventId;
        private String eventType;
        private ParkingDetail parkingDetail;
        private Payment payment;

        public Integer getParkingEventId() {
            return parkingEventId;
        }

        public void setParkingEventId(Integer parkingEventId) {
            this.parkingEventId = parkingEventId;
        }

        public String getEventType() {
            return eventType;
        }

        public void setEventType(String eventType) {
            this.eventType = eventType;
        }

        public ParkingDetail getParkingDetail() {
            return parkingDetail;
        }

        public void setParkingDetail(ParkingDetail parkingDetail) {
            this.parkingDetail = parkingDetail;
        }

        public Payment getPayment() {
            return payment;
        }

        public void setPayment(Payment payment) {
            this.payment = payment;
        }

    }


}
