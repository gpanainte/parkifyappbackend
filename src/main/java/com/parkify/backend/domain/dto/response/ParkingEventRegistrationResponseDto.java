package com.parkify.backend.domain.dto.response;

public class ParkingEventRegistrationResponseDto {
    private Integer parkingEventId;

    public Integer getParkingEventId() {
        return parkingEventId;
    }

    public void setParkingEventId(Integer parkingEventId) {
        this.parkingEventId = parkingEventId;
    }
}
