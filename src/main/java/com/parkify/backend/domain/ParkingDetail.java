package com.parkify.backend.domain;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalTime;

@Entity
public class ParkingDetail {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private BigDecimal id;

    @OneToOne
    private Parking parking;

    private LocalTime openFrom;

    private LocalTime openTo;

    private LocalTime freeFrom;

    private LocalTime freeTo;

    private BigDecimal hourlyFee;

    private String currency;

    public BigDecimal getId() {
        return id;
    }

    public LocalTime getOpenFrom() {
        return openFrom;
    }

    public void setOpenFrom(LocalTime openFrom) {
        this.openFrom = openFrom;
    }

    public LocalTime getOpenTo() {
        return openTo;
    }

    public void setOpenTo(LocalTime openTo) {
        this.openTo = openTo;
    }

    public LocalTime getFreeFrom() {
        return freeFrom;
    }

    public void setFreeFrom(LocalTime freeFrom) {
        this.freeFrom = freeFrom;
    }

    public LocalTime getFreeTo() {
        return freeTo;
    }

    public void setFreeTo(LocalTime freeTo) {
        this.freeTo = freeTo;
    }

    public BigDecimal getHourlyFee() {
        return hourlyFee;
    }

    public void setHourlyFee(BigDecimal hourlyFee) {
        this.hourlyFee = hourlyFee;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    @Override
    public String toString() {
        return "ParkingDetail{" +
                "id=" + id +
                ", openFrom=" + openFrom +
                ", openTo=" + openTo +
                ", freeFrom=" + freeFrom +
                ", freeTo=" + freeTo +
                ", hourlyFee=" + hourlyFee +
                ", currency='" + currency + '\'' +
                '}';
    }
}
