package com.parkify.backend.domain;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private BigDecimal id;

    private String name;

    @OneToOne(mappedBy = "user")
    private PaymentCard paymentCard;

    @Column(unique = true)
    private String plateNo;

    private BigDecimal tagId;

    @Column(unique = true)
    private String registrationId;

    public BigDecimal getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public PaymentCard getPaymentCard() {
        return paymentCard;
    }

    public void setPaymentCard(PaymentCard paymentCard) {
        this.paymentCard = paymentCard;
    }

    public String getPlateNo() {
        return plateNo;
    }

    public void setPlateNo(String plateNo) {
        this.plateNo = plateNo;
    }

    public BigDecimal getTagId() {
        return tagId;
    }

    public void setTagId(BigDecimal tagId) {
        this.tagId = tagId;
    }

    public String getRegistrationId() {
        return registrationId;
    }

    public void setRegistrationId(String registrationId) {
        this.registrationId = registrationId;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", plateNo='" + plateNo + '\'' +
                ", tagId='" + tagId + '\'' +
                ", registrationId='" + registrationId + '\'' +
                '}';
    }
}
