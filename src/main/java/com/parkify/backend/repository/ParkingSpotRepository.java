package com.parkify.backend.repository;

import com.parkify.backend.domain.ParkingSpot;
import org.springframework.data.repository.CrudRepository;

import java.math.BigDecimal;

public interface ParkingSpotRepository extends CrudRepository<ParkingSpot, BigDecimal> {

    public ParkingSpot findParkingSpotById(BigDecimal id);
}
