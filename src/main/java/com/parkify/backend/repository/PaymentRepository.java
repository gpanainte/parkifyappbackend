package com.parkify.backend.repository;

import com.parkify.backend.domain.Payment;
import org.springframework.data.repository.CrudRepository;

import java.math.BigDecimal;

public interface PaymentRepository extends CrudRepository<Payment, BigDecimal> {
}
