package com.parkify.backend.repository;

import com.parkify.backend.domain.ParkingEvent;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ParkingEventRepository extends CrudRepository<ParkingEvent, Integer> {

    public ParkingEvent findParkingEventById(Integer id);

    public List<ParkingEvent> findAll();
}
