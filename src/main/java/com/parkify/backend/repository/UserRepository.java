package com.parkify.backend.repository;

import com.parkify.backend.domain.User;
import org.springframework.data.repository.CrudRepository;

import java.math.BigDecimal;

public interface UserRepository extends CrudRepository<User, BigDecimal> {

    User findUserByPlateNo(String plateNo);

    User findUserByTagId(BigDecimal tagId);
}
