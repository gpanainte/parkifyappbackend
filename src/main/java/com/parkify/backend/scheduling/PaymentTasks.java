package com.parkify.backend.scheduling;

import com.google.firebase.messaging.FirebaseMessagingException;
import com.parkify.backend.repository.ParkingEventRepository;
import com.parkify.backend.service.ParkingService;
import com.parkify.backend.service.PushNotificationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;

@Component
public class PaymentTasks {

    private static final Logger log = LoggerFactory.getLogger(PaymentTasks.class);

    private ParkingEventRepository parkingEventRepository;
    private ParkingService parkingService;
    private PushNotificationService pushNotificationService;

    @Autowired
    public PaymentTasks(ParkingEventRepository parkingEventRepository,
                        ParkingService parkingService,
                        PushNotificationService pushNotificationService) {
        this.parkingEventRepository = parkingEventRepository;
        this.parkingService = parkingService;
        this.pushNotificationService = pushNotificationService;
    }

    @Scheduled(fixedRate = 2000)
    public void submitDuePayments() {
        parkingEventRepository.findAll()
                .stream()
                .filter(p -> p.getStatus().equals("ACCEPTED") &&
                        p.getDateTo() != null &&
                        Duration.between(p.getDateTo(), LocalDateTime.now()).toMillis() > 2000)
                .map(p -> parkingService.submitDuePayment(p))
                .forEach(p -> {
                    try {
                        pushNotificationService.sendPaymentEventNotification(p);
                    } catch (IOException | FirebaseMessagingException e) {
                        e.printStackTrace();
                    }
                });
    }
}
