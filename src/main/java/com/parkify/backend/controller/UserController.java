package com.parkify.backend.controller;

import com.parkify.backend.domain.User;
import com.parkify.backend.domain.dto.request.UpdateUserDataRequestDto;
import com.parkify.backend.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
public class UserController {

    private UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/users/{plateNo}")
    public User getUser(@PathVariable("plateNo") String plateNo) {
        return userService.getUser(plateNo);
    }

    @PostMapping(path = "/users/{plateNo}")
    public User updateUserData(@PathVariable("plateNo") String plateNo,
                                      @RequestBody UpdateUserDataRequestDto updateUserDataRequestDto) {
        return userService.updateUserData(plateNo, updateUserDataRequestDto);
    }
}
