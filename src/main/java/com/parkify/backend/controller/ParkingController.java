package com.parkify.backend.controller;

import com.google.firebase.messaging.FirebaseMessagingException;
import com.parkify.backend.domain.ParkingEvent;
import com.parkify.backend.domain.dto.request.ParkingRegisterEventRequestDto;
import com.parkify.backend.service.ParkingEventService;
import com.parkify.backend.service.ParkingService;
import com.parkify.backend.service.PushNotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.math.BigDecimal;

@RestController
@CrossOrigin
public class ParkingController {

    private ParkingService parkingService;
    private PushNotificationService pushNotificationService;
    private ParkingEventService parkingEventService;

    @Autowired
    public ParkingController(ParkingService parkingService,
                             PushNotificationService pushNotificationService,
                             ParkingEventService parkingEventService) {
        this.parkingService = parkingService;
        this.pushNotificationService = pushNotificationService;
        this.parkingEventService = parkingEventService;
    }

    @PostMapping("/parkings/{parkingId}/parkingSpots/{parkingSpotId}/registerEvent")
    public ParkingEvent registerParkingEvent(@PathVariable("parkingId") BigDecimal parkingId,
                                      @PathVariable("parkingSpotId") BigDecimal parkingSpotId,
                                      @RequestBody ParkingRegisterEventRequestDto parkingRegisterEventRequestDto) throws IOException, FirebaseMessagingException {
        ParkingEvent parkingEvent;
        if (parkingRegisterEventRequestDto.getParkingEventId() == null) {
            parkingEvent = parkingService.registerParkingEvent(parkingSpotId, parkingRegisterEventRequestDto.getTagId());
            pushNotificationService.sendRegisterEventNotification(parkingEvent);
        } else {
            parkingEvent = parkingService.updateParkingEvent(parkingRegisterEventRequestDto.getParkingEventId());
        }
        return parkingEvent;
    }

    @PostMapping("/parkingEvents/{parkingEventId}/accept")
    public ParkingEvent acceptParkingEvent(@PathVariable("parkingEventId") Integer parkingEventId) {
        return parkingService.acceptParkingEvent(parkingEventId);
    }
}
